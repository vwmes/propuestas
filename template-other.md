# Title of the proposal

Brief introduction and motivation of the same.

## Type of activity

Describe the type of the activity.

## Description

Short (couple of paragraphs) description about what the talk is about.

## Target audience

Who should Attend? 

## Speaker(s)

Who is going to give the talk? What do you/they do? (brief bio about yourself) What talks have you
given before?

### Contact(s)

* Nome: contact

For "Name", use the full name. For "contact", use an email address (format "user @ domain"), or the GitLab user name (format "user @ GitLab). In any case, have into account that these addresses will be used for contacting you, so you better check them frequently ;-)

## Requirements for attendees

Describe the requirements to participate in the activity. For example, bring a cell phone, an umbrella and a flashlight.

## Requirements for the organization

Describe the material facilities you need from the organization. For example, a room with tables and chairs, a computer room, a blackboard...

## Observations

Any other relevant observations.

## Conditions

* [ ] I agree to follow the [code of conduct](https://eslib.re/2019/conducta/) and request this acceptance from the attendees and speakers.
* [ ] At least one person among those proposing will be present on the day scheduled for the talk.