[Hay una [versión en español](README.md) de este documento]

# Call for contributions for esLibre 2020

[ Please forward this call for contributions to anyone who might find it interesting ]

The [esLibre conference](https://eslib.re)  is a meeting of people interested in free technologies, focused on sharing knowledge and experience around them. The next edition will be held at Universidad Rey Juan Carlos (Madrid) on 5th and 6th June 2020.

In esLibre we intend to have a yearly conference recovering and maintaining the idea of a community, in which we can take advantage of the experience of the old glories and the enthusiasm of new people. We want to achieve a technical environment, but one that does not forget, and explains, how important it is for technology to be free.

We also want to open the conference to a wider community interested in free culture, perhaps with less focus on technology, but with equal interest in sharing.

## Topics

Free software and open source software is the main topic of esLibre. It also covers other free technologies (for example, free hardware), and the less technological world of free knowledge. In general, topics directly related to works (software, hardware, cultural works, etc.) that are distributed under free licenses, according to the [OSI definition of open source](https://opensource.org/osd) or the [FSF definition of free software](https://www.gnu.org/philosophy/free-sw.html), or that meet the [definition of free cultural work](https://freedomdefined.org/Definition) are considered to fit perfectly into esLibre.

Although many free software, and free works are distributed commercially, we want to keep esLibre as a forum that is neutral towards commercial issues. In general, we will not accept talks, workshops or devrooms that specifically promote a commercial offer.

## Types of contributions

There are different types of contributions that you can propose:

* Talk. Standard presentation, in regular format (about 25 minutes) or as a lightning talk (about 10 minutes). The organization will be able to propose changes in the format of any of the talks, for organizational reasons or due to the content. As an exception, a longer format may be proposed for a talk.

* Room (devroom). Organization of the program of a room, which will run in parallel with other activities of the conference. Normally a devroom will be organized by and/or for a community, and will usually be specialized in one topic (although it can also be generalist). Just as an example, in 2019 we had the following devrooms: Free Software at the University; Privacy, Decentralization and Digital Sovereignty; Perl; Computer Science and Math.
 
* Workshop. Practical, "hands on" presentation. It can be in many formats, from demonstrations that attendees can follow, to introductory sessions to a technology, where attendees can experiment with it.
 
* Other. Use your imagination! Propose other formats, we are interested in exploring other ways of sharing knowledge.

In the templates to propose each type of contribution you can find some more details

## Dates and deadlines

* Deadline for devroom proposals: March 2nd
* Deadline for proposals for talks, workshops and others: March 16th
* Deadline for room program: April 30th
* EsLibre conference: June 5th-6th

All dates are at 23:59 (Madrid time).

## How to propose your contribution

The call for contributions is open to everyone. The way to propose a contribution will be through a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html) (pull request) in this repository.

For each type of contribution you have a template in the root folder of this repository. Copy it to the current year's folder, within the corresponding subfolder (`charlas` for talks, `salas` for devrooms, `talleres` for workshops, `otras` for other). Use a file name that is not already used, and that reflects the title of your proposal. Fill it in and make a merge request that includes it, to propose your activity.

Anyone will be able to see these proposals, and if they want, comment on them. The organization of esLibre will comment and accept (or not) those that seem convenient, according to your idea about the conference. If you propose a contribution, be alert to the comments you may receive, they might include questions and requests for clarification that might be convenient to answer.

If you have problems of any kind, or don't know how to open a merge request, please [open an issue in this repository](https://gitlab.com/eslibre/charlas/issues/new). In order to do this, you'll need to use a GitLab account.